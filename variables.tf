variable "aws_iam_user_name" {
  description = "The user's name. The name must consist of upper and lowercase alphanumeric characters with no spaces. You can also include any of the following characters: =,.@-_.. User names are not distinguished by case."
  type        = string
  default     = "ses-user"
}

variable "aws_iam_user_path" {
  description = "Path in which to create the user."
  type        = string
  default     = "/"
}

variable "aws_iam_user_policy_name" {
  description = "The name of the policy. If omitted, Terraform will assign a random, unique name."
  type        = string
  default     = "ses-allow-all"
}
variable "aws_region" {
  description = "The name of the AWS region."
  type        = string
}
