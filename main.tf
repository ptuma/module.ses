resource "aws_iam_user" "ses_user" {
  name = var.aws_iam_user_name
  path = var.aws_iam_user_path
}

data "aws_iam_policy_document" "ses_allow_all" {
  statement {
    effect = "Allow"
    resources = [
      "*",
    ]
    actions = [
      "ses:SendRawEmail"
    ]
  }
}

resource "aws_iam_user_policy" "ses_user_policy" {
  name = var.aws_iam_user_policy_name
  user = aws_iam_user.ses_user.name
  policy = data.aws_iam_policy_document.ses_allow_all.json
}

resource "aws_iam_access_key" "ses_user_key" {
  user = aws_iam_user.ses_user.name
}

module "convert_key_to_password" {
  source  = "matti/resource/shell"
  command = "python3 secret2password.py --secret ${aws_iam_access_key.ses_user_key.secret} --region ${var.aws_region}"
}
