output "ses_user_key_id" {
  description = "The user AWS access key"
  value = aws_iam_access_key.ses_user_key.id
}

output "iam_access_key_secret" {
  description   = "the access key secret"
  value         = aws_iam_access_key.ses_user_key.secret
}


output "ses_user_secret" {
  description = "The user password in right format for SES"
  value = module.convert_key_to_password.stdout
}
